#include <iostream>


void drawsquare(const unsigned int& size)
{
	int m_input = size+1;
	int m_size = -1;
	for (int c = 0; c < m_input; c++) m_size += 2;

	for (int i = 0; i < m_input; i++)
	{
		for (int j = 0; j < m_size; j++)
		{
			if (j>(m_size/2-i) && j<(m_size/2+i))
			{
				std::cout << "*";
			}else
			{
				std::cout << " ";
			}
		}
		std::cout << std::endl;
	}
	for (int i = 0; i < 3; i++)
	{
		for (int c = 0; c <= m_size/2; c++)
		{
			if (c == m_size/2)
			{
				std::cout << "|" << std::endl;
			}
			else
			{
				std::cout << " ";
			}
		}
	}


}
int main()
{
	int num;
	do
	{
	std::cout << "type a number between 1 and 24, type 0 to exit" << std::endl;
	std::cout << "tree size:  ";
	std::cin >> num;
	if (num > 0 && num < 25)
	{
		drawsquare(num);
	}
	else
	
		std::cout << "I said between 1 and 24!" << std::endl;
    }
	while (num !=0);
}