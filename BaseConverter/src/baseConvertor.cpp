#include <iostream>
#include <string>

std::string BaseConverter(const unsigned int& num, const unsigned int& base)//base menor que 37
{
	int storage[100];
	int m_result,m_mod,input = num, cnt = 0;
	char abc;

	std::string output = "\n";
	do
	{
		m_result = input / base;
		m_mod = input % base;
		storage[cnt] = m_mod;
		input = m_result;
		cnt++;
	} while (m_result > 0);

	storage[cnt] = m_result;
	
	for (int i=cnt;i>=0;i--)
	{
		if (storage[i]>=10)
		{
			 abc = ('A' + storage[i] - 10);
			 output += abc;
		}
		else
		{
			 output += std::to_string(storage[i]);	
		}
	}
	return output;
}

int main()
{
	int num, base;
	do
	{
		std::cout << "number to convert and base to convert" << std::endl;
		std::cout << "___   ___   minimum base: 1 maximun base: 36" << std::endl;
		std::cin >> num >> base;
		if (base > 1 && base < 37)
			std::cout << BaseConverter(num, base) << std::endl;
	} while (base > 1 && base < 37);
}