#include <iostream>
#include <vector>
bool isActive = true;

bool checkval(const std::vector<int> v, const int& n, int i)
{
	if (v[i] == n)
		return true;
	if (v[i] != v.back() && n > v[i])//check if this is the last element of the vector, or if the number is smaller that the current number in the vector(as the vector is ordered)
		return checkval(v, n, i + 1);
	return false;
}

bool binarySearch(const std::vector<int> v, const int& n)
{
	return checkval(v, n, 0);
}

int main()
{
	int num = 20;
	int num2 = 5;


	std::vector<int> test = { 2, 4, 6, 8, 10, 20, 100, 101 };

	if (binarySearch(test, num))
		std::cout << "The number " << num << " is inside the vector\n";
	else
		std::cout << "The number " << num << " is not inside the vector\n";

	std::cout << "\n\n";
	if (binarySearch(test, num2))
		std::cout << "The number " << num2 << " is inside the vector\n";
	else
		std::cout << "The number " << num2 << " is not inside the vector\n";

	std::cin.get();
	return 0;
}