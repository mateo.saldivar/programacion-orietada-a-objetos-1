#include <iostream>

void printController(const int& value)
{
    if (value & 1)
        std::cout << "CUADRADO ";
    if (value & 2)
        std::cout << "EQUIS ";
    if (value & 4)
        std::cout << "CIRCULO ";
    if (value & 8)
        std::cout << "TRIANGULO ";
    if (value & 16)
        std::cout << "LEFT ";
    if (value & 32)
        std::cout << "DOWN ";
    if (value & 64)
        std::cout << "RIGHT ";
    if (value & 128)
        std::cout << "UP ";
}

int main()
{
    for (int i = 0; i < 20; i++)
    {
        unsigned char value = rand() % 255;
        std::cout << value << "  -> ";
        printController(value);
        std::cout << "\n";
    }

}
