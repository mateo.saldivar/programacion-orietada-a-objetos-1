#include <iostream>

struct _Node {
	int value;
	_Node* prev;
	_Node* next;

	//constructor : Es una funcion que se ejecuta automaticamente cuando se crea un objeto 
	//y nunca mas se puede volver a ejecutar
	//El constructor siempre se llama igual que la estructura

	_Node(const int& value)
	{
		this->value = value;
		this->next = nullptr;
		this->prev = nullptr;
	}

};

typedef struct _Node Node;

typedef Node* ptr_Node;


//Completar
void insert(ptr_Node& head, const int& value)
{
	ptr_Node newNode = new Node(value);
	newNode->value = value;
	newNode->next = nullptr;
	newNode->prev = nullptr;
	//ligado
	if (head == nullptr)//si el primer nodo esta ocupado o no
	{
		head = newNode;
	}
	else
	{
		//necesito encontrar el ultimo nodo y ligarlo con el nuevo
		Node* temp = head;
		while (temp->next != nullptr)
		{
			temp = temp->next;
		}

		temp->next = newNode;
		newNode->prev = temp;
	}

}

//Completar
void printList(ptr_Node& head)
{
	ptr_Node readNode = head;
	ptr_Node lastNode = head;
	while (readNode != nullptr)
	{
		std::cout << readNode->value;
		readNode = readNode->next;

		if (readNode != nullptr)
		{
			std::cout << ", ";
			lastNode = readNode;
		}
			
		
	}std::cout << "\n";

	while (lastNode != nullptr)
	{
		std::cout << lastNode->value;
		lastNode = lastNode->prev;
		if (lastNode != nullptr)
			std::cout << ", ";
	}std::cout << "\n";
}

void main(void)
{
	ptr_Node Head = nullptr;
	for (int i = 1; i <= 15; ++i)
		insert(Head, i * 10);
	printList(Head);
}
