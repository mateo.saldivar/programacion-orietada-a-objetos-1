#include <iostream>

typedef struct _node {//declaro un tipo de datos llamado Node
	int value;
	struct _node* next;
} Node;

typedef Node* ptr_Node;//defino ptr_node como un pointer a node

ptr_Node Head;//mi pointer se llama head

void printList(ptr_Node& node)
{
	ptr_Node readNode = node;
	while (readNode != nullptr)
	{
		std::cout << readNode->value;
		readNode = readNode->next;
		if (readNode != nullptr)
			std::cout << ", ";
	}std::cout << "\n";
}

void insertInto(ptr_Node& head, int value)//meto una direccion de memoria y un valor a insertar
{

	//creacion e inicializacion del nodo
	ptr_Node newNode = new Node();
	newNode->value = value;
	newNode->next = nullptr;

	//ligado
	if (head == nullptr)//si el primer nodo esta ocupado o no
	{
		head = newNode;
	}
	else
	{
		//necesito encontrar el ultimo nodo y ligarlo con el nuevo
		Node* temp = head;
		while (temp->next != nullptr)
			temp = temp->next;

		temp->next = newNode;
	}


}

//jamboard: https://jamboard.google.com/d/189Jerf8yj_QD9IOBju8ySwxqXVada6DSLUmkBws8u5o/viewer?f=2

void main(void)
{
	Head = nullptr;//inicializo pointer
	for (int i = 1; i <= 15; ++i)
		insertInto(Head, i * 10);
	
	printList(Head);
}
