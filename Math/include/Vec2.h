#pragma once

#include "Vector.h"

class Vec2 : public Vector
{
public:
	Vec2();
	Vec2(const float& x, const float& y);
	Vec2(const Vec2& rhs);

	bool operator== (const Vec2& rhs);
	Vec2& operator= (const Vec2& rhs);
	Vec2& operator+ (const Vec2& rhs);
	Vec2& operator* (const float& rhs);
	float operator* (const Vec2& rhs);//producto punto
	Vec2& operator- (const Vec2& rhs);
	Vec2& operator/ (const float& rhs);
	//virtual void printValues() override;
};
