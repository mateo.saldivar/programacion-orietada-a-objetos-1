#pragma once
#include "Vector.h"

class Vec3 : public Vector
{
public:
	Vec3();
	Vec3(const float& x, const float& y, const float& z);
	Vec3(const Vec3& rhs);

	bool operator== (const Vec3& rhs);
	Vec3& operator= (const Vec3& rhs);
	Vec3& operator+ (const Vec3& rhs);
	//
	////EJEMPLO DE POLIMORFISMO
	////El metodo "operator*" se comporta de difetente forma dependiendo de los parametros de entrada
	Vec3& operator* (const float& rhs);
	Vec3& operator* (const Vec3& rhs);  //IMPLEMENTAR EL PRODUCTO CRUZ
	Vec3& operator/ (const float& rhs);
	//virtual void printValues();

};