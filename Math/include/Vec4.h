#pragma once

#include "Vector.h"

class Vec4 : public Vector
{
public:
	Vec4();
	Vec4(const float& x, const float& y, const float& z, const float& w);
	Vec4(const Vec4& rhs);

	bool operator== (const Vec4& rhs);
	Vec4& operator= (const Vec4& rhs);
	Vec4& operator+ (const Vec4& rhs);
	//
	////EJEMPLO DE POLIMORFISMO
	////El metodo "operator*" se comporta de difetente forma dependiendo de los parametros de entrada
	Vec4& operator* (const float& rhs);
	float operator* (const Vec4& rhs);  //IMPLEMENTAR EL PRODUCTO CRUZ/NO SE OUEDE NO HAY EN 4D, implemente el punto y el cruz en 3D
	Vec4& operator/ (const float& rhs);
	//virtual void printValues();

};
