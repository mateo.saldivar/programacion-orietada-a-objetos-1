#pragma once

#include <vector>
#include <iostream>

class Vector
{
//private:  solo esta clase ve sus propiedades y metodos
//public:  todos pueden ver las propiedades y metodos
//protected: hace las propiedades y metodos privados, 
//           pero las clases que heredan las ven como publicas
protected: 
	std::vector <float> v;

public:
	//"Metodo vitual puro" porque no va a tener implementacion
	//Al haber al menos un metodo abstracto, la clase se convierte 
	//en Clase Abstracta
	//Ojo: No se pueden crear instancias de una clase abstracta

	virtual void printValues()
	{
		std::cout << "\n{ ";
		for (int i = 0; i < v.size(); ++i) 
		{
			std::cout << v[i];
			if (i != v.size()-1) 
			{
				std::cout << ", ";
			}
		}
		std::cout << " }\n ";
	};

};

