#include <iostream>

#include "../include/Vec2.h"
#include "../include/Vec4.h"
#include "../include/Vec3.h"


int main()
{
	//Crear una clase Vec2, Vec3, Vec4 para un vector de 2, 3, y 4 dimensiones, y definirle el operador +, - entre vectores de su mismo tipo
	// 	   ejemplo: Vec4 v3 = v1 + v2;
	//y tambien definir operadores *, / con flotantes
	//     ejemplo: v1 = v1 / 100.0; => v1(x/100.0, y/100.0, z/100.0, w/100.0)
	
	Vec2 v1 = Vec2(2,4);
	Vec2 v2 = Vec2(12,3);

	v1 = v1 + v2;
	v1.printValues();
	//v1.v.push_back(12);  ERROR: v es una propiedad protegida, eso quiere decir que, en este contexto se comporta como privada
	//Vector v;  //No se puede por ser clase abstracta
	Vec4 v4; 
	v4.printValues();
	//v.printValues(); No se puede porque es clase abstracta



	Vec4 v4_1(10, 20, 30, 1);
	Vec4 v4_2(10, 10, 10, 10);
	//Vec4 v4_3 = v4_1 * v4_2;   //producto cruz
	Vec3 v3_1(1,34,12);
	v3_1.printValues();
	v4_2 = v4_2 * 100;
	v4_2.printValues();


	v1 = v1 + v2;
	v2 = v2 - v1;
	v1.printValues();

	Vec3 v3_a(1,2,3);
	Vec3 v3_b(4, 5, 6);
	v3_a.printValues();
	v3_b.printValues();
	v3_a = v3_a * v3_b;
	v3_a.printValues();
}

