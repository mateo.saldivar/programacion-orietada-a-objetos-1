#include  "Vec2.h"

Vec2::Vec2()
{
	v = { 0.0f, 0.0f };
}

Vec2::Vec2(const float& x, const float& y)
{
	v.push_back(x);
	v.push_back(y);
}

Vec2::Vec2(const Vec2& rhs) : Vec2()
{
	*this = rhs; //Wow havcemos uso del operador de asignacion construiodo por nosotros!!!
}

bool Vec2::operator== (const Vec2& rhs)
{
	return (v[0] == rhs.v[0] && 
		    v[1] == rhs.v[1]);
}

Vec2& Vec2::operator= (const Vec2& rhs)
{
	if (&rhs != this)
	{
		v[0] = rhs.v[0];
		v[1] = rhs.v[1];
	}

	return *this;
}

//  A  +  B
// lhs   rhs
Vec2& Vec2::operator+ (const Vec2& rhs)
{
	v[0] = v[0] + rhs.v[0];
	v[1] = v[1] + rhs.v[1];

	return *this;
}



Vec2& Vec2::operator* (const float& rhs)
{
	v[0] = v[0] * rhs;
	v[1] = v[1] * rhs;

	return *this;
}

float Vec2::operator*(const Vec2& rhs)
{
	return v[0]*rhs.v[0]+v[1]*rhs.v[1];
}

Vec2& Vec2::operator-(const Vec2& rhs)
{
	v[0] = v[0] - rhs.v[0];
	v[1] = v[1] - rhs.v[1];

	return *this;
}

Vec2& Vec2::operator/(const float& rhs)
{
	v[0] = v[0] / rhs;
	v[1] = v[1] / rhs;

	return *this;
}
