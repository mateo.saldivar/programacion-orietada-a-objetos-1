#include "../include/Vec3.h"

Vec3::Vec3()
{
	v = { 0.f,0.f,0.f };
}

Vec3::Vec3(const float& x, const float& y, const float& z)
{
	v.push_back(x);
	v.push_back(y);
	v.push_back(z);
}

Vec3::Vec3(const Vec3& rhs)
{
	*this = rhs;
}

bool Vec3::operator==(const Vec3& rhs)
{
	return (v[0] == rhs.v[0] &&
		v[1] == rhs.v[1] &&
		v[2] == rhs.v[2]);
}

Vec3& Vec3::operator=(const Vec3& rhs)
{
	if (&rhs != this)
	{
		v[0] = rhs.v[0];
		v[1] = rhs.v[1];
		v[2] = rhs.v[2];
	}

	return *this;
}

Vec3& Vec3::operator+(const Vec3& rhs)
{
	v[0] = v[0] + rhs.v[0];
	v[1] = v[1] + rhs.v[1];
	v[2] = v[2] + rhs.v[2];
	return *this;
}

Vec3& Vec3::operator*(const float& rhs)
{
	v[0] = v[0] * rhs;
	v[1] = v[1] * rhs;
	v[2] = v[2] * rhs;
	return *this;
}

Vec3& Vec3::operator*(const Vec3& rhs)
{
	float x, y, z;
	x = v[1] * rhs.v[2] - v[2] * rhs.v[1];
	y = v[2] * rhs.v[0] - v[0] * rhs.v[2];
	z = v[0] * rhs.v[1] - v[1] * rhs.v[0];
	v[0] = x;
	v[1] = y;
	v[2] = z;
	return *this;
}


Vec3& Vec3::operator/(const float& rhs)
{
	v[0] = v[0] / rhs;
	v[1] = v[1] / rhs;
	v[2] = v[2] / rhs;
	return *this;
}
