#include "Vec4.h"

Vec4::Vec4()
{
	v = {0.f,0.f,0.f,0.f};
}

Vec4::Vec4(const float& x, const float& y, const float& z, const float& w)
{
	v.push_back(x);
	v.push_back(y);
	v.push_back(z);
	v.push_back(w);
}

Vec4::Vec4(const Vec4& rhs)
{
	*this = rhs;
}

bool Vec4::operator==(const Vec4& rhs)
{
	return (v[0] == rhs.v[0] &&
		v[1] == rhs.v[1] &&
		v[2] == rhs.v[2] &&
		v[3] == rhs.v[3]);
}

Vec4& Vec4::operator=(const Vec4& rhs)
{
	if (&rhs != this)
	{
		v[0] = rhs.v[0];
		v[1] = rhs.v[1];
		v[2] = rhs.v[2];
		v[3] = rhs.v[3];
	}

	return *this;
}

Vec4& Vec4::operator+(const Vec4& rhs)
{
	v[0] = v[0] + rhs.v[0];
	v[1] = v[1] + rhs.v[1];
	v[2] = v[2] + rhs.v[2];
	v[3] = v[3] + rhs.v[3];

	return *this;
}

Vec4& Vec4::operator*(const float& rhs)
{
	v[0] = v[0] * rhs;
	v[1] = v[1] * rhs;
	v[2] = v[2] * rhs;
	v[3] = v[3] * rhs;

	return *this;
}

float Vec4::operator*(const Vec4& rhs)//producto punto porque no se pueden establecer productos cruz de 4 dimensiones
{
	return v[0] * rhs.v[0] + v[1] * rhs.v[1] + v[2] * rhs.v[2] + v[3] * rhs.v[3];
}




Vec4& Vec4::operator/(const float& rhs)
{
	v[0] = v[0] / rhs;
	v[1] = v[1] / rhs;
	v[2] = v[2] / rhs;
	v[3] = v[3] / rhs;

	return *this;
}


