#include <iostream>
#include <string>


bool isPalindrom_v1(const std::string& text)
{
	std::string rev = "";
	for (int i = text.length() - 1; i >= 0; --i)
	{
		rev += text[i];
	}
	return rev == text;
}

bool isPalindrom_v2(const std::string& text)
{

	int last = text.length() - 1;

	for (int i = 0; i <= last; ++i)
	{
		if (text[i] != text[last - i])
		{
			return false;
		}
	}
	return true;
}


bool isPalindrom_v3(const std::string& text)
{
	if (text.length() < 2) 
		return true;
	int last = text.length() - 1;

	for (int i = 0; i <= last; ++i)
	{
		if (i < (last / 2)+1)
		{
			if (text[i] != text[last - i])
			{
				return false;
			}
		}
		else
			return true;
	}
}


bool isPalindrom_v4(const std::string& text)
{
	if (text.length() < 2)
		return true;
	
	std::string spaceless = "";

	for (int i = 0; i < text.length(); ++i)
	{
		if(text[i]!=' ')
			spaceless += text[i];
	}

	int last = spaceless.length() - 1;

	for (int i = 0; i <= last; ++i)
	{
		if (i < (last / 2) + 1)
		{
			if (spaceless[i] != spaceless[last - i])
			{
				return false;
			}
		}
		else
			return true;
	}
}


int main()
{
	std::string word;
	while (word != "quit")
	{
		std::cout << "writte your palindrome   ";
		std::getline(std::cin,word);
		if (isPalindrom_v4(word))
			std::cout << "true\n";
		
	};
}