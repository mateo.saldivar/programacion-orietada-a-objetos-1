#include <iostream>
#include "BinaryTree.h"

void main()
{
	BinaryTree tree;

	tree.insert(5);
	tree.insert(3);
	tree.insert(7);
	tree.insert(4);
	tree.insert(6);
	tree.insert(9);
	tree.insert(8);

	

	tree.printInorder();
	std::cout << std::endl;
	tree.printReversed();
	std::cout << std::endl;
	tree.printPreorder();
	std::cout << std::endl;
	tree.printPostorder();
}



