#include "BinaryTree.h"
#include <iostream>

void BinaryTree::insertRecursive(ptrNode& root, ptrNode& newNode)
{
	if (root == nullptr)  //caso 1: no existe el nodo de referencia
		root = newNode;
	else if (newNode->value < root->value) //caso 2: se tiene que insertar por la izq.
		insertRecursive(root->left, newNode);
	else //caso 3: se tiene que insertar por la derecha por ser mayor o igual
		insertRecursive(root->right, newNode);
}

void BinaryTree::printRecursiveInfix(ptrNode& root)
{
	if (root == nullptr)
		return;

	printRecursiveInfix(root->left);
	std::cout << root->value << std::endl;
	printRecursiveInfix(root->right);
}

void BinaryTree::printReversed(ptrNode& root)
{
	if (root->right != nullptr)
		printReversed(root->right);
	std::cout << root->value << std::endl;
	if (root->left != nullptr)
		printReversed(root->left);
}

void BinaryTree::insert(const int& value)
{
	Node* temp = new Node(value);
	insertRecursive(root, temp);
}

void BinaryTree::printInorder()
{
	printRecursiveInfix(root);//primero izq, luego el de referencia, luego el derecho
}

void BinaryTree::printPreorder()
{
	printPreorder(root);
}

void BinaryTree::printPreorder(ptrNode& root)//primero el de referencia, luego el izq, y luego el der
{
	std::cout << root->value << std::endl;
	if (root->left != nullptr)
	{
		printPreorder(root->left);
	}
	if (root->right != nullptr)
	{
		printPreorder(root->right);
	}
}

void BinaryTree::printPostorder(ptrNode& root)//primero el de la izquierda, luego el de la derecha, luego el de de referencia	
{

	if (root->left != nullptr)
		printPostorder(root->left);
	if (root->right != nullptr)
		printPostorder(root->right);
	std::cout << root->value << std::endl;
}

void BinaryTree::printPostorder()
{
	printPostorder(root);
}

void BinaryTree::printReversed()
{
	if (root != nullptr)
		printReversed(root);
}

BinaryTree::BinaryTree()
{
	//root(nullptr);
}