#pragma once
#include "Node.h"


class LinkedList
{
private:
	Node* head,
		* tail;
	unsigned int size;

public:
	LinkedList();
	void PrintList();
	void Insert(const int& value);

	/*
	* Este metodo debe insertar el nuevo valor
	* manteniendo la lista siempre ordenada
	*/
	void SortedInsert(const int& value);
};