#pragma once
#include <iostream>

class Node
{
public:
	//propiedades (ya no les llamo variables, porque forman parte de la clase
	int value;
	Node* prev;
	Node* next;

	//metodo es lo mismo que una funcion, pero esta definido dentro de una clase

	//OJO el codigo de estos metodos esta en src/Node.cpp
	Node(const int& value);
	void PrintValue();
};