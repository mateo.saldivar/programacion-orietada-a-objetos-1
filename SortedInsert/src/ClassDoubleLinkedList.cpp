#include <iostream>
#include "../include/Node.h"
#include "../include/LinkedList.h"


void main(void)
{
	//Node n(5);  //n es una variable que contiene una "instancia" de la clase Node
	//n.value = 10;   //no se puede acceder a value, porque es una propiedad privada
	//n.PrintValue();
	//std::cout << std::endl;
	/*
	LinkedList list;
	
	list.Insert(1);
	list.Insert(2);
	list.Insert(3);
	list.Insert(4);
	list.Insert(5);
	list.Insert(6);
	list.Insert(7);

	list.PrintList();
	*/
	///////////////////////
	LinkedList sortedList;

	sortedList.SortedInsert(10);
	sortedList.SortedInsert(1);
	sortedList.SortedInsert(43);
	sortedList.SortedInsert(-100);
	sortedList.SortedInsert(7);
	sortedList.SortedInsert(2);
	sortedList.SortedInsert(1);
	sortedList.SortedInsert(-123);
	sortedList.SortedInsert(1324);
	sortedList.SortedInsert(12);
	sortedList.SortedInsert(3);
	sortedList.SortedInsert(4);
	sortedList.SortedInsert(0);

	//debe imprimir la lista ordenada
	// -100, 1, 1, 2, 10, 43
	sortedList.PrintList();
}