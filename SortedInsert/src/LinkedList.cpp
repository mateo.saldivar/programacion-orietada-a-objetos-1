#include "../include/LinkedList.h"

LinkedList::LinkedList() : head(nullptr),
tail(nullptr),
size(0)
{
}

void LinkedList::PrintList()
{ //imprima todos los vals "valor, valor, valor... enter"
	Node* temp = head;
	while (temp != nullptr)
	{
		temp->PrintValue();
		std::cout << ", ";
		temp = temp->next;
	}

	std::cout << std::endl;
}

void LinkedList::Insert(const int& value)
{
	Node* newNode = new Node(value);

	if (head == nullptr)
	{ //no hay lista, este es el primer node
		head = newNode;
		tail = newNode;
		size = 1;
	}
	else
	{ //ya hay uno o mas nodos
		tail->next = newNode;
		newNode->prev = tail;
		tail = newNode;
		++size;
	}
}


//Tarea, completatar el metodo
void LinkedList::SortedInsert(const int& value)
{
	Node* newNode = new Node(value);
	
	if (head == nullptr)  //caso 1 : la lista es vacia
	{
		head = tail = newNode;
	}
	else if (newNode->value < head->value)//es el menor de la lista
	{
		newNode->next = head;
		head->prev = newNode;
		head = newNode;
	}
	else if (newNode->value>tail->value)//es el mayor de la lista
	{
		tail->next = newNode;
		newNode->prev = tail;
		tail = newNode;
	}
	else//esta en medio de la lista
	{
		Node* temp = head;
		for (int i = 0; i < size; ++i)
		{
			if (newNode->value > temp->value)
			{
				temp = temp->next;
			}
			else
			{
				newNode->next = temp;
				newNode->prev = temp->prev;
				newNode->prev->next = newNode;
				temp->prev = newNode;
				break;
			}
		}
		
	}
	++size;
}