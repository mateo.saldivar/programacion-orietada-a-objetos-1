#include "../include/Node.h"

Node::Node(const int& value) : value(value),
prev(nullptr),
next(nullptr)
{
}

void Node::PrintValue()
{
	std::cout << value;
}