#include <vector>
#include <iostream>

std::vector<int> v1 = {100,200,300,400,500}, v2 = { 12,11,10,9,8,7,6,5,4,3,2,1 };

std::vector<int> sort(std::vector<int> v1, std::vector<int> v2)
{
    std::vector<int> v3(v1.size()+v2.size());
    //quiero primero preparar mi espacio de trabajo, es mas eficiente a andar buscando en memoria cada vez que agrego un valor a mi vector
    for (int i = 0; i < v3.size(); ++i) {
        if (i < v1.size())
            v3[i] = v1[i];
        else
            v3[i] = v2[i - v1.size()];
    }
    int temp, j = 0;//switch sort by MSCF
    for (int i = 0; i < v3.size(); ++i)
        for (j = 0; j < v3.size()-1;++j) {
            if (v3[j] > v3[j + 1])
            {
                temp = v3[j];
                v3[j] = v3[j + 1];
                v3[j + 1] = temp;
            }
        }
    return v3;
}

void printVector(std::vector<int> v)
{
    for (int i = 0; i < v.size(); ++i)
    {
        std::cout << v[i] << "  ";
    }
    std::cout << "\n";

}

int main()
{
    printVector(v1);
    printVector(v2);
   std::vector<int> result = sort(v1,v2);
   std::cout << "/////////////////////////////////////////\n";
   printVector(result);
   std::cin.get();

}
//version 1.1 funciona pero no me gusta, quiero anidar el while, el if quiza se quede
/*    while (j < v3.size())
    {
        for (int i = 0; i < v3.size(); ++i)
        {
            if (i<v3.size() - 1 && v3[i] > v3[i + 1])
            {
                temp = v3[i];
                v3[i] = v3[i + 1];
                v3[i + 1] = temp;
            }
        }
        ++j;
    }
 //version 1.2 funciona
    for (int i = 0; i < v3.size(); ++i)
    {
        j=0;
        while (j < v3.size() - 1)
        {
            if (v3[j] > v3[j + 1])
            {
                temp = v3[j];
                v3[j] = v3[j + 1];
                v3[j + 1] = temp;
            }
            ++j;
        }
    }
    
    */