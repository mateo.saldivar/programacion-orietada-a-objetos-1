#include <iostream>
#include <vector>


/*
*
		  i
a{ 6, 8, 10, 12, 20, 22}

			   j
b{ 1, 3, 5, 7, 9 }


C  1, 3, 5, 6, 7, 8, 9

*/

// v1 y v2 TIENEN que estar ordenados
std::vector<int> merge(const std::vector<int>& v1, const std::vector<int>& v2)
{
	std::vector<int> result;

	int i = 0, j = 0;//i v1 y j v2
	bool loop = true;
	do
	{
		if (v1[i] <= v2[j])
		{
			result.push_back(v1[i++]);
		}
		else
		{
			result.push_back(v2[j++]);
		}

		if (j >= v2.size())
		{
			for (i; i < v1.size(); ++i)
			{
				result.push_back(v1[i]);
			}
			loop = false;
		}
		else if (i >= v1.size())
		{
			for (j; j < v2.size(); ++j)
			{
				result.push_back(v2[j]);
			}
			loop = false;
		}

	} while (loop);
	return result;
}


/*
	bubble sort

	1 comparar un elemento contra TODOS los que estan adelante e intercambia las pociones solamente cuando el que esta adelante es mas peque�o
	2. repetir el paso 1, con el elemento siguiente, hasta que lleguemos al penultimo elemento

	ejemplo:
	PASO 1:
	i  j
	3, 2, 1  ..s� es mas peque�o, intercambiamos (queda 2, 3, 1)

	i     j
	2, 3, 1   ..s� es mas peque�o, intercambiamos (Queda 1, 3, 2). En este punto ya se comparo el 2 contra todos los de adelante , pasamos al paso 2
	Al finalizar el paso 1, nos qued�     1, 3, 2

	Repetir paso 1 (segun dice el paso 2):
	   i  j
	1, 3, 2  ..s� es mas peque�o, intercambiamos (Queda 1, 2, 3)



	Nota: como se intercambian los valores entre 2 variables?
	int a = 3, b = 5;
	int temp = a;
	a = b; //en este momento  a y b valen  5
	b = temp;  // en este momento a = 5, b = 3;

*/

std::vector<int> sort(const std::vector<int>& v)
{
	std::vector<int> vR(v.size());
	for (int i = 0; i < v.size(); ++i)
		vR[i] = v[i];

	int temp, j = 0;//switch sort by MS
	for (int i = 0; i < v.size(); ++i)
		for (j = 0; j < v.size() - 1; ++j)
			if (vR[j] > vR[j + 1]) {
				temp = vR[j];
				vR[j] = vR[j + 1];
				vR[j + 1] = temp;
			}
	return vR;
}

// No se espera que v1 y v2 esten ordenados, pero el resultado si debe estar ordenado
std::vector<int> unsorted_merge(const std::vector<int>& v1, const std::vector<int>& v2)
{//opcion 3.
	std::vector<int> v3(v1.size() + v2.size());
	//quiero primero preparar mi espacio de trabajo, es mas eficiente a buscar en memoria cada vez que agrego un valor a mi vector
	for (int i = 0; i < v3.size(); ++i)
	{
		if (i < v1.size())
			v3[i] = v1[i];
		else
			v3[i] = v2[i - v1.size()];
	}
	int temp, j = 0;//switch sort by MS
	for (int i = 0; i < v3.size(); ++i)
		for (j = 0; j < v3.size() - 1; ++j)
			if (v3[j] > v3[j + 1]) {
				temp = v3[j];
				v3[j] = v3[j + 1];
				v3[j + 1] = temp;
			}
	return v3;
}


//Esta funcion debe imprimir en pantalla todos los elementos del vector separados por coma
//ejemplo: para el vector {1, 2, 3, 4, 5}
//Imprimir en pantalla:
//  1, 2, 3, 4, 5,
// nota, la ultima coma puede ir o no.
void printVector(const std::vector<int>& v)
{
	for (int i = 0; i < v.size(); ++i)
	{
		std::cout << v[i] << ", ";
	}
	std::cout << "\n";
}

/*

	Opcion 1 (Ojo, esta es igual que la Opcion 3, pero un poco menos eficiente, prefiere la opcion 2 o la opcion 3)
	1. hacer v3 = resultado de merge juntando v1 y v2
	2. ordenar v3 con un algoritmo de ordenamiento que hagamos


	Opcion 2:
	1. crear un algoritmo de ordenamiento
	2. ordenar v1
	3. ordenar v2
	4. hacer v3 = resultado de merge juntando v1 y v2


	Opcion 3.
	1. Hacer v3 = v1 + v2
	2. crear algoritmo de ordenamiento
	3. ordenar v3

*/

void main(void)
{

	std::vector<int> a{2,90,1,3,1,9}, b{ 1, 5, 210, 22, 1, 24, 16 };

	std::vector<int> d = sort(a);
	std::vector<int> c = unsorted_merge(a, b);   //c debe tener todos los elementos de a y b ordenados

	printVector(c);
	std::cin.get();

}